const fs = require('fs');
const acorn = require('acorn');

const file = process.argv[2];

const content = fs.readFileSync(file, { encoding: 'utf-8' });

const tree = acorn.tokenizer(content);

const getTokenString = (data) => {
  switch (data.type.label) {
    case 'string':
      return data.value;
    case 'name':
      return data.value ? data.value : data.type.label;
    case ',':
      return '|';
    default:
      return data.type.label;
  }
};

const data = [];

let started = false;
let exp = '';
for (let token of tree) {
  // iterate over the tokens
  if (!started && token.type.label === 'name' && token.value === 'Text') {
    started = true;
    exp += token.value;
  }
  else if (started && token.type.label === ')') {
    started = false;
    exp += ')';
    data.push(exp);
    exp = '';
  } else if (started) {
    console.log(token)
    exp += getTokenString(token);
  }
  if (token.type === acorn.tokTypes.eof) {
    break;
  }
}

console.log(data.filter(f => {
  return f.match(/^\w{1,}\(/);
}));
