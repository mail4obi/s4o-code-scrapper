const fs = require('fs');
const { forEach } = require('lodash');

const { getFunctions } = require('./uitls');

const getFileData = (fn, file) => {
  try {
    const result = {};
    const content = fs.readFileSync(file);
    const lines = getFunctions(content, fn);

    const argsRegex = /\((.*)\)/;

    for (let i = 0; i < lines.length; i++) {
      const [, args] = argsRegex.exec(lines[i]);
      const argList = args.split('|').map((r) => {
        return r.trim().replace(/[(')]/g, '');
      });
      const [arg1, arg2, arg3, ...otherArgs] = argList;

      if (arg1 !== 'number') {
        result[`@${arg1.toLowerCase()}_${arg2.toLowerCase().replace(/\s/g, '_')}`] = arg2;
      }

      if (otherArgs.length > 0) {
        const others = otherArgs.join();
        const obj = others.substr(1, others.length - 2).split(',');
        forEach(obj, (p) => {
          const [key, val] = p.split(':');
          result[`@${key.toLowerCase()}_${String(val).toLowerCase()}`] = val;
        })
      }
    }

    return result;
  } catch (error) {
    console.error('processing file', file);
    console.error(error.message);
  }
}

const defaultTask = (fn, dirs, inputFile, outputFile) => {
  console.log(dirs);

  const output = [];

  const scan = (dir) => {
    const dirList = fs.readdirSync(dir);
    for (let j = 0; j < dirList.length; j++) {
      const file = `${dir}/${dirList[j]}`;
      if (fs.lstatSync(file).isDirectory()) {
        scan(file);
      } else {
        const rr = getFileData(fn, file);
        output.push(rr);
      }
    }
  }

  const directories = dirs.split(',');
  for (let i = 0; i < directories.length; i++) {
    scan(directories[i]);
  }

  const combined = Object.assign({}, ...output);
  const keys = Object.keys(combined);
  keys.sort();
  const finalResult = {};
  for (let i = 0; i < keys.length; i++) {
    finalResult[keys[i]] = combined[keys[i]];
  }

  if (fs.existsSync(inputFile)) {
    try {
      const inputData = fs.readFileSync(inputFile);
      const inputObj = JSON.parse(inputData);
      Object.assign(finalResult, inputObj);
    } catch (error) {
    }
  }

  const fileContent = JSON.stringify(finalResult, null, 2);
  console.log(fileContent);
  fs.writeFileSync(outputFile, fileContent);
}

module.exports = {
  defaultTask,
};
