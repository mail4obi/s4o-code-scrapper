const acorn = require('acorn');
const jsx = require("acorn-jsx");

const getTokenString = (data) => {
  switch (data.type.label) {
    case 'string':
      return data.value;
    case 'name':
      return data.value ? data.value : data.type.label;
    case ',':
      return '|';
    default:
      return data.type.label;
  }
};

const getFunctions = (content, pattern) => {
  const tree = acorn.Parser.extend(jsx()).tokenizer(content);
  const data = [];

  let started = false;
  let exp = '';
  for (let token of tree) {
    // iterate over the tokens
    if (!started && token.type.label === 'name' && String(token.value).match(pattern)) {
      started = true;
      exp += token.value;
    }
    else if (started && token.type.label === ')') {
      started = false;
      exp += ')';
      data.push(exp);
      exp = '';
    } else if (started) {
      exp += getTokenString(token);
    }
    if (token.type === acorn.tokTypes.eof) {
      break;
    }
  }

  return data.filter(f => {
    return f.match(/^\w{1,}\(/);
  });
};

module.exports = {
  getFunctions
};
